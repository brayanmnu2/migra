package com.pragma.servicio;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pragma.entidad.DatosPersonales;
import com.pragma.entidad.ExperienciaLaboralExterna;
import com.pragma.repositorio.DatosPersonalesRepository;
import com.pragma.repositorio.ExperienciaLaboralExternaRepository;

@Service
public class MigracionExperienciaLaboralExternaServicio {
	
	@Autowired
	ExperienciaLaboralExternaRepository experienciaLaboralExternaRepository;
	
	@Autowired
	DatosPersonalesRepository datosPersonalesRepository;
	
	private static final Logger log = LoggerFactory.getLogger(MigracionExperienciaLaboralExternaServicio.class);

	int cuantos = 1;
	
	public void migrarInformacionLaboralExterna() {

		String rutaJson = "C:\\Users\\Jhonatan.Acosta\\Desktop\\RDS-PROD\\INFORMACION LABORAL\\rds-laboral.json";
		List<ExperienciaLaboralExterna> listicaExterna = new ArrayList<>();
		
		try {
			JSONParser parser = new JSONParser();

			JSONArray listXpPragma = (JSONArray) parser.parse(new FileReader(rutaJson));

			for (Object o : listXpPragma) {

				ExperienciaLaboralExterna xpPragma = new ExperienciaLaboralExterna();
				JSONObject xp = (JSONObject) o;
				System.out.println("//////////////////////////////////////////////////////////////////////-------------------CUANTOS : "+ cuantos);

				String empresa = (String) xp.get("Company");
				if (!empresa.equalsIgnoreCase("Pragma S.A.")) {
					log.info(empresa);
					xpPragma.setEmpresa(empresa);

					String rol = (String) xp.get("Role");
					log.info(rol);
					xpPragma.setRol(rol);

					String fechaIngreso = (String) xp.get("StartDate");
					LocalDate fechitaIngreso = null;
					if (!fechaIngreso.isBlank()) {
						DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

						Date date = formatter.parse(fechaIngreso);

						Timestamp timestamp = new Timestamp(date.getTime());

						fechitaIngreso = timestamp.toLocalDateTime().toLocalDate();
					}
					xpPragma.setFechaDeIngreso(fechitaIngreso);

					String fechaFinalizacion = (String) xp.get("EndDate");
					LocalDate fechitaFinal = null;
					if (!fechaFinalizacion.isBlank()) {
						DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

						Date date = formatter.parse(fechaFinalizacion);

						Timestamp timestamp = new Timestamp(date.getTime());

						fechitaFinal = timestamp.toLocalDateTime().toLocalDate();
					}

					xpPragma.setFechaDeFinalizacion(fechitaFinal);

					String tiempoDeXp = (String) xp.get("ExperienceTime");
					xpPragma.setTiempoDeExperiencia(tiempoDeXp);

					String logros = (String) xp.get("Tasks");
					log.info(logros);
					xpPragma.setLogros(logros);

					String idUsuario = (String) xp.get("UserId");
					Optional<DatosPersonales> datosPeOpt = datosPersonalesRepository.findByGoogleId(idUsuario);
					if (datosPeOpt.isPresent()) {
						String personOriginal = datosPeOpt.get().getId();
						xpPragma.setIdUsuario(personOriginal);
						log.info("ID TABLA USUARIO : " + personOriginal);

					}
					
					cuantos++;
					listicaExterna.add(xpPragma);
 
				}

			}
			experienciaLaboralExternaRepository.saveAll(listicaExterna);

		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
