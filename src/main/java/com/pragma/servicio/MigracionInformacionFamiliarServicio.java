package com.pragma.servicio;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pragma.entidad.DatosPersonales;
import com.pragma.entidad.Genero;
import com.pragma.entidad.InformacionFamiliar;
import com.pragma.entidad.Parentesco;
import com.pragma.repositorio.DatosPersonalesRepository;
import com.pragma.repositorio.InformacionFamiliarRepository;
import com.pragma.repositorio.ParentescoRepository;

@Service
public class MigracionInformacionFamiliarServicio {

	@Autowired
	ParentescoRepository parentescoRepository;

	@Autowired
	InformacionFamiliarRepository informacionFamiliarRepository;

	@Autowired
	DatosPersonalesRepository datosPersonalesRepository;

	private static final Logger log = LoggerFactory.getLogger(MigracionInformacionFamiliarServicio.class);

	int cuantos = 1;

	public void migrarInformacionFamiliar() {

		List<InformacionFamiliar> listicaMelaFamiliar = new ArrayList<>();
		String ruta = "C:\\Users\\Jhonatan.Acosta\\Desktop\\RDS-PROD\\INFORMACION FAMILIAR\\rds-familiar.json";

		try {
			JSONParser parser = new JSONParser();

			JSONArray listFamiliar = (JSONArray) parser.parse(new FileReader(ruta));

			for (Object familia : listFamiliar) {
				InformacionFamiliar infoFamilia = new InformacionFamiliar();
				JSONObject family = (JSONObject) familia;

				System.out.println(
						"//////////////////////////////////////////////////////////////////////-------------------CUANTOS :"
								+ cuantos);

				String fechaNacimiento = (String) family.get("BirthDate");
				LocalDate fechita = null;

				if (!fechaNacimiento.isBlank()) {
					DateFormat formatter = null;
					if (fechaNacimiento.contains("-")) {
						formatter = new SimpleDateFormat("yyyy-MM-dd");
					} else {
						formatter = new SimpleDateFormat("yyyy/MM/dd");
					}
					// DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

					Date date = formatter.parse(fechaNacimiento);

					Timestamp timestamp = new Timestamp(date.getTime());

					fechita = timestamp.toLocalDateTime().toLocalDate();
				}

				infoFamilia.setFechaNacimiento(fechita);

				String genero = (String) family.get("Gender");
				infoFamilia.setGenero(Genero.convertirGenero(genero));
				log.info("GENERO: " + genero);

				String apellidos = (String) family.get("LastName");
				infoFamilia.setApellidos(apellidos);
				log.info("APELLIDOS: " + apellidos);

				String nombre = (String) family.get("Name");
				infoFamilia.setNombres(nombre);
				log.info("NOMBRE: " + nombre);

				String parentesco = (String) family.get("Relationship");
				log.info("RELATIONSHIP: " + parentesco);
				Optional<Parentesco> parentescoOpt = parentescoRepository.findByNombre(parentesco);
				Parentesco parentescoOriginal = parentescoOpt.get();
				infoFamilia.setIdParentesco(parentescoOriginal);
				log.info("PARENTESCO: " + parentescoOriginal.getNombre());

				String idUsuario = (String) family.get("UserId");
				Optional<DatosPersonales> datosPeOpt = datosPersonalesRepository.findByGoogleId(idUsuario);
				if (datosPeOpt.isPresent()) {
					String personOriginal = datosPeOpt.get().getId();
					infoFamilia.setIdUsuario(personOriginal);
					log.info("ID TABLA USUARIO : " + personOriginal);

				}


				cuantos++;
				listicaMelaFamiliar.add(infoFamilia);

			}

			informacionFamiliarRepository.saveAll(listicaMelaFamiliar);

		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
