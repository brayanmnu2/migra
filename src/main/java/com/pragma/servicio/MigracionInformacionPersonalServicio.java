package com.pragma.servicio;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pragma.entidad.DatosPersonales;
import com.pragma.entidad.Estado;
import com.pragma.entidad.EstadoCivil;
import com.pragma.entidad.GrupoSanguineo;
import com.pragma.entidad.Profesion;
import com.pragma.entidad.TallaCamisa;
import com.pragma.entidad.TipoDocumento;
import com.pragma.entidad.UbicacionGeografica;
import com.pragma.entidad.Vicepresidencia;
import com.pragma.repositorio.DatosPersonalesRepository;
import com.pragma.repositorio.ProfesionRepository;
import com.pragma.repositorio.TipoDocumentoRepository;
import com.pragma.repositorio.UbicacionGeograficaRepository;
import com.pragma.repositorio.VicepresidenciaRepository;

@Service
public class MigracionInformacionPersonalServicio {

	@Autowired
	UbicacionGeograficaRepository ubicacionGeograficaRepository;

	@Autowired
	VicepresidenciaRepository vicepresidenciaRepository;

	@Autowired
	TipoDocumentoRepository tipoDocumentoRepository;

	@Autowired
	ProfesionRepository profesionRepository;

	@Autowired
	DatosPersonalesRepository datosPersonalesRepository;

	private static final Logger log = LoggerFactory.getLogger(MigracionInformacionPersonalServicio.class);

	int cuantos = 1;

	public void migrarPragmaticos() {

		List<DatosPersonales> listicaMela = new ArrayList<>();
	    String ruta = "C:\\Users\\Jhonatan.Acosta\\Desktop\\Migraciones\\ultimo.json";
		try {
			JSONParser parser = new JSONParser();

			JSONArray listPragmaticos = (JSONArray) parser.parse(new FileReader(ruta));
			for (Object o : listPragmaticos) {
				DatosPersonales datosPersonales = new DatosPersonales();
				JSONObject person = (JSONObject) o;
				System.out.println(
						"//////////////////////////////////////////////////////////////////////-------------------CUANTOS : "
								+ cuantos);

				String idUsuario = (String) person.get("UserId");
				datosPersonales.setGoogleId(idUsuario);
				log.info("idUsuario = " + idUsuario);

				String nombres = (String) person.get("UserName");
				datosPersonales.setNombres(nombres);
				log.info("nombre(s) = " + nombres);

				String numeroIdentificacion = (String) person.get("NumberId");
				datosPersonales.setIdentificacion(numeroIdentificacion);

				String apellidos = (String) person.get("LastName");
				datosPersonales.setApellidos(apellidos);

				String correoPersonal = (String) person.get("Email");
				datosPersonales.setCorreoPersonal(correoPersonal);

				String correoEmpresarial = (String) person.get("CorporateEmail");
				datosPersonales.setCorreoEmpresarial(correoEmpresarial);

				String telefonoFijo = (String) person.get("Phone");
				datosPersonales.setTelefonoFijo(telefonoFijo);

				String telefonoCelular = (String) person.get("CellPhone");
				datosPersonales.setTelefonoCelular(telefonoCelular);
				
				String fechaIngreso = (String) person.get("DateAdmission");
				LocalDate fechasaIngreso = null;
				
				if(fechaIngreso.isBlank() || fechaIngreso == null) {
					datosPersonales.setFechaIngreso(null);
				} else {
					DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

					Date dateIngre = formatter.parse(fechaIngreso);

					Timestamp timestampIng = new Timestamp(dateIngre.getTime());

					fechasaIngreso = timestampIng.toLocalDateTime().toLocalDate();
					
					datosPersonales.setFechaIngreso(fechasaIngreso);
				}

				String fechaNacimiento = (String) person.get("BirthDate");
				LocalDate fechita = null;

				if (!fechaNacimiento.isBlank()) {
					DateFormat formatter = null;
					if (fechaNacimiento.contains("-")) {
						formatter = new SimpleDateFormat("yyyy-MM-dd");
					} else {
						formatter = new SimpleDateFormat("yyyy/MM/dd");
					}
					// DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

					Date date = formatter.parse(fechaNacimiento);

					Timestamp timestamp = new Timestamp(date.getTime());

					fechita = timestamp.toLocalDateTime().toLocalDate();
				}

				datosPersonales.setFechaNacimiento(fechita);

				String direccion = (String) person.get("Address");
				datosPersonales.setDireccion(direccion);

				String nombreContactoDeEmergencia = (String) person.get("ContactName");
				datosPersonales.setNombreContactoEmergencia(nombreContactoDeEmergencia);

				String parentescoContactoDeEmergencia = (String) person.get("ContactRelationship");
				datosPersonales.setParentescoContactoEmergencia(parentescoContactoDeEmergencia);

				String telefonoContactoEmergencia = (String) person.get("ContactNumber");
				datosPersonales.setTelefonoContactoEmergencia(telefonoContactoEmergencia);

				String tarjetaProfesional = (String) person.get("ProfessionalCard");
				datosPersonales.setTarjetaProfesional(tarjetaProfesional);

				String perfilProfesional = (String) person.get("Summary");
				datosPersonales.setPerfilProfesional(perfilProfesional);

				String conocimientosTecnicos = (String) person.get("SkillsDescription");
				datosPersonales.setConocimientosTecnicos(conocimientosTecnicos);

				String tallaCamisa = (String) person.get("Size");
				datosPersonales.setTallaCamisa(TallaCamisa.convertirTallaCamisa(tallaCamisa));

				String estadoCivil = (String) person.get("CivilStatus");
				datosPersonales.setEstadoCivil(EstadoCivil.convertirEstadoCivil(estadoCivil));

				String tipoDeSangre = (String) person.get("BloodType");
				String rh = (String) person.get("Rh");
				datosPersonales.setGrupoSanguineo(GrupoSanguineo.convertir(tipoDeSangre, rh));

				String estado = (String) person.get("IsActive");
				datosPersonales.setEstado(Estado.convertirEstado(estado));

				String tipoDocumento = (String) person.get("TypeId");
				log.info("TIPITO: " + tipoDocumento);
				if (tipoDocumento == "") {
					tipoDocumento = "NO DEFINIDA";
				}
				Optional<TipoDocumento> tipoDocumentoOpt = tipoDocumentoRepository.findByNombre(tipoDocumento);
				TipoDocumento tipoDocumentoOrigi = tipoDocumentoOpt.get();
				datosPersonales.setIdTipoDocumento(tipoDocumentoOrigi);
				log.info("tipoDocumentoOrigi = " + tipoDocumentoOrigi.getNombre());

				String profesion = (String) person.get("Profession");
				if (profesion == "") {
					profesion = "INDEFINIDA";
				}
				if (profesion.equals("Ingeniero de sistemas") || profesion.equalsIgnoreCase("Ingeniero informático")
						|| profesion.equalsIgnoreCase("Ingeniero en informática")
						|| profesion.equalsIgnoreCase("Ingeniero de sistemas y telecomunicaciones")
						|| profesion.equalsIgnoreCase("Ingeniero de sistemas y computación")
						|| profesion.equalsIgnoreCase("Ingeniería de sistemas y computación")
						|| profesion.equalsIgnoreCase("Ingeniería de sistemas")
						|| profesion.equalsIgnoreCase("Ingeniero de sistemas e informática")) {
					profesion = "Ingeniería de sistemas e información";
				}

				if (profesion.equals("Ingeniero en telecomunicaciones")) {
					profesion = "Ingeniería electrónica y telecomunicaciones";
				}

				if (profesion.equals("Tecnólogo en sistematizacion de datos")) {
					profesion = "Tecnología en sistematización de datos";
				}

				if (profesion.equals("Administrador de sistemas informáticos")
						|| profesion.equalsIgnoreCase("Estudiante")
						|| profesion.equalsIgnoreCase("Lic. dirección y producción de cine, radio y televisión.")
						|| profesion.equalsIgnoreCase("Ingeniero de producción")) {
					profesion = "INDEFINIDA";

				}

				if (profesion.equalsIgnoreCase("Ingeniero industrial")) {
					profesion = "Ingeniería industrial";
				}

				if (profesion.equalsIgnoreCase("Tecnólogo en analisis y desarrollo de sistemas de información")
						|| profesion
								.equalsIgnoreCase("Tecnólogo en análisis y desarrollo de sistemas de información")) {
					profesion = "Tecnología en sistemas de información";
				}

				if (profesion.equalsIgnoreCase("Ingeniero electronico")
						|| profesion.equalsIgnoreCase("Ingeniero en telemática")
						|| profesion.equalsIgnoreCase("Ingeniero electrónico")) {
					profesion = "Ingeniería electrónica y telecomunicaciones";
				}

				if (profesion.equalsIgnoreCase("Contador público")) {
					profesion = "Contaduría pública";
				}

				if (profesion.equalsIgnoreCase("Tecnólogo en gestion de redes de datos")) {
					profesion = "Tecnología en gestión de redes de datos";
				}

				if (profesion.equalsIgnoreCase("Ingeniero en multimedia")) {
					profesion = "Ingeniería de multimedia";
				}

				if (profesion.equalsIgnoreCase("Administradora de empresas")) {
					profesion = "Administración de empresas";
				}

				if (profesion.equalsIgnoreCase("Ingeniero de procesos")) {
					profesion = "Ingeniería de procesos";
				}

				if (profesion.equalsIgnoreCase("Ingeniero financiero y de negocios")) {
					profesion = "Ingeniería financiera y de negocios";
				}

				if (profesion.equalsIgnoreCase("Ingeniero mecánico")) {
					profesion = "Ingeniería  mecánica";
				}
				
				if(profesion.equalsIgnoreCase("Técnica en programación de software")) {
					profesion = "Técnico en programación de sistemas de información";
				}

				log.info("profesion = " + profesion);
				Optional<Profesion> profesionOpt = profesionRepository.findByNombre(profesion);
				Profesion profesionOrigi = profesionOpt.get();

				datosPersonales.setIdProfesion(profesionOrigi);
				log.info("profesionOrigi = " + profesionOrigi.getNombre());

				String lugarDeNacimiento = (String) person.get("BirthCity");
				log.info("LUGAR NACIMIENTO: " + lugarDeNacimiento);

				if (lugarDeNacimiento.isBlank()) {
					lugarDeNacimiento = "0";
				}

				if (lugarDeNacimiento.equals("48798") || lugarDeNacimiento.equals("48800")
						|| lugarDeNacimiento.equals("48801") || lugarDeNacimiento.equals("48802")
						|| lugarDeNacimiento.equals("CO-NSA-001")) {
					lugarDeNacimiento = "48417";
				}

				if (lugarDeNacimiento.equalsIgnoreCase("48493") || lugarDeNacimiento.equalsIgnoreCase("48562")
						|| lugarDeNacimiento.equalsIgnoreCase("48500") || lugarDeNacimiento.equalsIgnoreCase("32604")
						|| lugarDeNacimiento.equalsIgnoreCase("48569") || lugarDeNacimiento.equals("48533")) {
					lugarDeNacimiento = "0";
				}

				if (lugarDeNacimiento.equalsIgnoreCase("CO-NSA-518") || lugarDeNacimiento.equals("48829")
						|| lugarDeNacimiento.equals("48807") || lugarDeNacimiento.equals("CO-NSA-001")) {
					lugarDeNacimiento = "12601";
				}

				if (lugarDeNacimiento.equalsIgnoreCase("48799")) {
					lugarDeNacimiento = "12688";
				}

				Optional<UbicacionGeografica> ubicacionOpt = ubicacionGeograficaRepository
						.findByLegacy(lugarDeNacimiento);

				UbicacionGeografica ubicacionGeo = ubicacionOpt.get();
				datosPersonales.setIdLugarDeNacimiento(ubicacionGeo);
				datosPersonales.setNacionalidad(ubicacionGeo);
				log.info(ubicacionGeo.getIdLegacy());

				String lugarDeResidencia = (String) person.get("ResidentCity");
				log.info("RESIDENCIA: " + lugarDeResidencia);
				if (lugarDeResidencia.equals("")) {
					lugarDeResidencia = "0";
				}

				if (lugarDeResidencia.equals("48798") || lugarDeResidencia.equals("48800")
						|| lugarDeResidencia.equals("48802") || lugarDeResidencia.equals("CO-NSA-673")) {
					lugarDeResidencia = "48417";
				}

				if (lugarDeResidencia.equalsIgnoreCase("Itagüí")) {
					lugarDeResidencia = "12590";

				}

				if (lugarDeResidencia.equalsIgnoreCase("Bogotá") || lugarDeResidencia.equals("48799")) {
					lugarDeResidencia = "12688";
				}

				if (lugarDeResidencia.equalsIgnoreCase("Medellin") || lugarDeResidencia.equals("CO-NSA-518")
						|| lugarDeResidencia.equalsIgnoreCase("48801") || lugarDeResidencia.equals("Medellín")
						|| lugarDeResidencia.equalsIgnoreCase("Caldas, Antioquia")) {
					lugarDeResidencia = "12601";

				}

				if (lugarDeResidencia.equalsIgnoreCase("48493") || lugarDeResidencia.equalsIgnoreCase("48562")
						|| lugarDeResidencia.equalsIgnoreCase("48500") || lugarDeResidencia.equalsIgnoreCase("32604")
						|| lugarDeResidencia.equalsIgnoreCase("48569") || lugarDeResidencia.equalsIgnoreCase("48536")
						|| lugarDeResidencia.equalsIgnoreCase("48544") || lugarDeResidencia.equalsIgnoreCase("48533")
						|| lugarDeResidencia.equals("trujillo") || lugarDeResidencia.equalsIgnoreCase("48538")) {
					lugarDeResidencia = "0";
				}

				if (lugarDeResidencia.equalsIgnoreCase("Envigado")) {
					lugarDeResidencia = "12578";
				}

				if (lugarDeResidencia.equalsIgnoreCase("Rionegro")) {
					lugarDeResidencia = "12617";
				}

				if (lugarDeResidencia.equalsIgnoreCase("sabaneta")) {
					lugarDeResidencia = "12619";
				}

				Optional<UbicacionGeografica> ubicacionResOpt = ubicacionGeograficaRepository
						.findByLegacy(lugarDeResidencia);
				UbicacionGeografica ubicacionResGeo = ubicacionResOpt.get();
				datosPersonales.setIdLugarDeResidencia(ubicacionResGeo);
				log.info(ubicacionResGeo.getIdLegacy());

				datosPersonales.setFechaUltimaActualizacion(LocalDateTime.now());
			
				String vice = (String) person.get("VicePresidency");
				log.info("VICE: " + vice);
				if (vice.equals("") || vice.equals("Cero")) {
					vice = "NO DEFINIDA";
				}
				Optional<Vicepresidencia> viceOptional = vicepresidenciaRepository.findByNombre(vice);
				Vicepresidencia viceP = viceOptional.get();
				datosPersonales.setIdVicepresidencia(viceP);
				log.info("VICE" + viceP.getNombre());

				cuantos++;
				listicaMela.add(datosPersonales);

			}

			datosPersonalesRepository.saveAll(listicaMela);

		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
