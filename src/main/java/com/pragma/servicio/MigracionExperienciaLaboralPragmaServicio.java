package com.pragma.servicio;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.pragma.entidad.DatosPersonales;
import com.pragma.entidad.ExperienciaLaboralPragma;
import com.pragma.repositorio.DatosPersonalesRepository;
import com.pragma.repositorio.ExperienciaLaboralPragmaRepository;

@Component
public class MigracionExperienciaLaboralPragmaServicio {

	@Autowired
	ExperienciaLaboralPragmaRepository experienciaLaboralPragmaRepository;

	@Autowired
	DatosPersonalesRepository datosPersonalesRepository;
	
	private static final Logger log = LoggerFactory.getLogger(MigracionExperienciaLaboralPragmaServicio.class);
	
	int cuantos = 1;
	public void migrarInformacionLaboralPragma() {
		String rutaJson = "C:\\Users\\Jhonatan.Acosta\\Desktop\\RDS-PROD\\INFORMACION LABORAL\\rds-laboral.json";
		List<ExperienciaLaboralPragma> listicaPragma = new ArrayList<>();

		try {
			JSONParser parser = new JSONParser();

			JSONArray listXpPragma = (JSONArray) parser.parse(new FileReader(rutaJson));

			for (Object o : listXpPragma) {

				JSONObject xp = (JSONObject) o;

				String company = (String) xp.get("Company");
				if (company.equalsIgnoreCase("Pragma S.A.")) {
					JSONArray listInternXpPragma = (JSONArray)parser.parse((String) xp.get("Projects"));

					for (Object p : listInternXpPragma) {
						ExperienciaLaboralPragma xpPragma = new ExperienciaLaboralPragma();
						JSONObject xpInterna = (JSONObject) p;
						JSONObject xpM = (JSONObject) xpInterna.get("M");
						JSONObject rolQueEs = (JSONObject) xpM.get("role");
						JSONObject fechaFinalQueEs = (JSONObject) xpM.get("endDate");
						JSONObject tiempoXpQueEs = (JSONObject) xpM.get("experienceTime");
						JSONObject proyectoQueEs = (JSONObject) xpM.get("project");
						JSONObject fechaInicialQueEs = (JSONObject) xpM.get("startDate");
						JSONObject tareasQueEs = (JSONObject) xpM.get("tasks");

						/*
						 * String idUsuario = (String) xp.get("UserId"); System.out.println("ID DYNAMO"
						 * + idUsuario); Optional<DatosPersonales> datosPersonalesOpt =
						 * datosPersonalesRepository.findByGoogleId(idUsuario); DatosPersonales datico =
						 * datosPersonalesOpt.get(); xpPragma.setIdUsuario(datico.getId());
						 * System.out.println("ID MAPEADO");
						 */
						System.out.println("CUANTOS : " + cuantos );
						String role = (String) rolQueEs.get("S");
						xpPragma.setRol(role);
						log.info(role);

						String fechaFinal = (String) fechaFinalQueEs.get("S");
						LocalDate fechitaFinal = null;
						if (fechaFinal != null) {
							DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

							Date date = formatter.parse(fechaFinal);

							Timestamp timestamp = new Timestamp(date.getTime());

							fechitaFinal = timestamp.toLocalDateTime().toLocalDate();
						}
						xpPragma.setFechaDeFinalizacion(fechitaFinal);

						String tiempoQueEs = (String) tiempoXpQueEs.get("S");
						xpPragma.setTiempoDeExperiencia(Long.valueOf(tiempoQueEs));
						log.info(tiempoQueEs);

						String proyectoEs = (String) proyectoQueEs.get("S");
						xpPragma.setProyecto(proyectoEs);
						log.info(proyectoEs);

						String fechaInicial = (String) fechaInicialQueEs.get("S");
						LocalDate fechitaI = null;
						if (!fechaInicial.isBlank()) {
							DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

							Date date = formatter.parse(fechaInicial);

							Timestamp timestamp = new Timestamp(date.getTime());

							fechitaI = timestamp.toLocalDateTime().toLocalDate();
						}
						xpPragma.setFechaDeIngreso(fechitaI);

						String tareas = (String) tareasQueEs.get("S");
						xpPragma.setLogros(tareas);
						log.info(tareas);

						String idUsuario = (String) xp.get("UserId");
						Optional<DatosPersonales> datosPeOpt = datosPersonalesRepository.findByGoogleId(idUsuario);
						if (datosPeOpt.isPresent()) {
							String personOriginal = datosPeOpt.get().getId();
							xpPragma.setIdUsuario(personOriginal);
							log.info("ID TABLA USUARIO : " + personOriginal);

						}

						listicaPragma.add(xpPragma);
						cuantos++;

					}

				}

			}
			experienciaLaboralPragmaRepository.saveAll(listicaPragma);

		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
