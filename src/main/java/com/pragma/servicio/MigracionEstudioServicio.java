package com.pragma.servicio;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pragma.entidad.DatosPersonales;
import com.pragma.entidad.Estudio;
import com.pragma.repositorio.DatosPersonalesRepository;
import com.pragma.repositorio.EstudioRepository;

@Service
public class MigracionEstudioServicio {

	@Autowired
	EstudioRepository estudioRepository;
	
	@Autowired
	DatosPersonalesRepository datosPersonalesRepository;

	private static final Logger log = LoggerFactory.getLogger(MigracionEstudioServicio.class);

	int cuantos = 1;

	public void migrarEstudio() {
		List<Estudio> listicaEstudio = new ArrayList<>();
		String ruta = "C:\\Users\\Jhonatan.Acosta\\Desktop\\RDS-PROD\\INFORMACION ACAD - ESTUDIO\\rds-estudio.json";

		try {
			JSONParser parser = new JSONParser();

			JSONArray listEstudio = (JSONArray) parser.parse(new FileReader(ruta));

			for (Object estudio : listEstudio) {
				Estudio estudios = new Estudio();
				JSONObject study = (JSONObject) estudio;

				System.out.println(
						"//////////////////////////////////////////////////////////////////////-------------------CUANTOS :"
								+ cuantos);

				String nivel = (String) study.get("Level");
				estudios.setNivel(nivel);
				log.info("NIVEL: " + nivel);

				String idUsuario = (String) study.get("UserId");
				Optional<DatosPersonales> datosPeOpt = datosPersonalesRepository.findByGoogleId(idUsuario);
				if (datosPeOpt.isPresent()) {
					String personOriginal = datosPeOpt.get().getId();
					estudios.setIdUsuario(personOriginal);
					log.info("ID TABLA USUARIO : " + personOriginal);

				}

				String tituloObtenido = (String) study.get("Title");
				estudios.setTituloObtenido(tituloObtenido);
				log.info("TITULO OBTENIDO: " + tituloObtenido);

				String anioFinal = (String) study.get("Year");
				estudios.setAnioFinalizacion(anioFinal);
				log.info("AÑO FINALIZACION: " + anioFinal);

				String institucion = (String) study.get("Institute");
				estudios.setInstitucion(institucion);
				log.info("INSTITUCION: " + institucion);

				listicaEstudio.add(estudios);

				cuantos++;

			}
			estudioRepository.saveAll(listicaEstudio);

		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
