package com.pragma.servicio;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pragma.entidad.OtrosEstudios;
import com.pragma.repositorio.OtrosEstudiosRepository;

@Service
public class MigracionOtrosEstudiosServicio {
	
	@Autowired
	OtrosEstudiosRepository otrosEstudiosRepository;

	private static final Logger log = LoggerFactory.getLogger(MigracionOtrosEstudiosServicio.class);

	int cuantos = 1;

	public void migrarOtrosEstudios() {
		List<OtrosEstudios> listicaOtrosEstudio = new ArrayList<>();
		String ruta = "C:\\Users\\Jhonatan.Acosta\\Desktop\\Migraciones\\Migracion informacion academica(otros_estudios)\\infoOtrosEstudiosTodos.json";

		try {
			JSONParser parser = new JSONParser();

			JSONArray listOtroEstudio = (JSONArray) parser.parse(new FileReader(ruta));

			for (Object otroEstudio : listOtroEstudio) {
				OtrosEstudios otroEst = new OtrosEstudios();
				JSONObject otherStudy = (JSONObject) otroEstudio;

				System.out.println(
						"//////////////////////////////////////////////////////////////////////-------------------CUANTOS :"
								+ cuantos);

				String fechaExp = (String) otherStudy.get("ExpirationDate");
				LocalDate fechita = null;
				
				if(fechaExp == null) {
					otroEst.setFechaDeExpiracion(null);
				}
				        
				if (fechaExp != null) {
					DateFormat formatter = null;
					if (fechaExp.contains("-")) {
						formatter = new SimpleDateFormat("yyyy-MM-dd");
					} else {
						formatter = new SimpleDateFormat("yyyy/MM/dd");
					}
					
					Date date = formatter.parse(fechaExp);

					Timestamp timestamp = new Timestamp(date.getTime());

					fechita = timestamp.toLocalDateTime().toLocalDate();
					
					otroEst.setFechaDeExpiracion(fechita);
				}

				

				String idUsuario = (String) otherStudy.get("UserId");
				otroEst.setIdUsuario(idUsuario);
				log.info("ID USUARIO: " + idUsuario);
				
				String horas = (String) otherStudy.get("Hours");
				otroEst.setHoras(horas);
				log.info("HORAS: " + horas);
				
				String etiquetas = (String) otherStudy.get("Tags");
				otroEst.setEtiquetas(etiquetas);
				log.info("ETIQUETAS: " + etiquetas);
				
				String titulo = (String) otherStudy.get("Title");
				otroEst.setTitulo(titulo);
				log.info("TITULO: " + titulo);
				
				String anio = (String) otherStudy.get("Year");
				otroEst.setAnio(anio);
				log.info("AÑO: " + anio);
				
				String instituto = (String) otherStudy.get("Institute");
				otroEst.setInstituto(instituto);
				log.info("INSTITUTO: " + instituto);
				
				String urlCertificado = (String) otherStudy.get("FullPath");
				otroEst.setUrlCertificado(urlCertificado);
				log.info("URL CERTIFICADO: " + urlCertificado);
				

				listicaOtrosEstudio.add(otroEst);

				cuantos++;

			}
			otrosEstudiosRepository.saveAll(listicaOtrosEstudio);

		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
