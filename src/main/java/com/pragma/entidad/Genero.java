package com.pragma.entidad;

public enum Genero {

    MASCULINO,
    NO_ESPECIFICA,
    FEMENINO;

    public static Genero convertirGenero(String genero) throws Exception{
        if(genero.isEmpty() || genero == null){
            return null;
        }
        
        if(genero.isBlank()) {
        	return NO_ESPECIFICA;
        }

        for (Genero value : Genero.values()) {
            if(value.name().equalsIgnoreCase(genero)){
                return value;
            }

        }
        throw new Exception("Ocurrio un error con el genero");

    }

}
