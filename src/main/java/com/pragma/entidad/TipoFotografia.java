package com.pragma.entidad;

public enum TipoFotografia {
	

    REDONDA,
    CUADRADA;

    public static TipoFotografia convertirTipoFoto(String tipo) throws Exception{
        if(tipo.isEmpty() || tipo == null){
            return null;
        }

        for (TipoFotografia value : TipoFotografia.values()) {
            if(value.name().equalsIgnoreCase(tipo)){
                return value;
            }

        }
        throw new Exception("Ocurrio un error con el tipo de fotografia");

    }

}
