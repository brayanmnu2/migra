	package com.pragma.entidad;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;

@Entity
@Table(name = "profesion")
@Getter
public class Profesion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_profesion", unique = true, nullable = false)
    private Long idProfesion;

    @Column(name = "nombre" , nullable = false)
    private String nombre;

    @OneToMany(fetch = FetchType.LAZY , mappedBy = "idProfesion")
    private List<DatosPersonales> listDatos = new ArrayList<>();

}
