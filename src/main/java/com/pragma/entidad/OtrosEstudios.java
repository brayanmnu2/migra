package com.pragma.entidad;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "otros_estudios")
@Getter
@Setter
public class OtrosEstudios {
	
	@Id
	@Column(name = "id")
	@GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
	private String id;
	
	private LocalDate fechaDeExpiracion;
	
	private String idUsuario;
	
	private String horas;
	
	private String etiquetas;
	
	private String titulo;
	
	private String anio;
	
	private String instituto;
	
	private String urlCertificado;

}
