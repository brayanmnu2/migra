package com.pragma.entidad;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;

@Entity
@Table(name = "tipo_documento")
@Getter
public class TipoDocumento {
	

    @Id
    @Column(name = "id_tipo_documento" , unique = true , nullable = false , length = 50)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idTipoDocumento;

    @Column(name = "abreviatura" , nullable = false)
    private String abreviatura;

    @Column(name = "nombre" , nullable = false)
    private String nombre;

    @OneToMany(fetch = FetchType.LAZY , mappedBy = "idTipoDocumento")
    private List <DatosPersonales> listDatos = new ArrayList<>();


}
