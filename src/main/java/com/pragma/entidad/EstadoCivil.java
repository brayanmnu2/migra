package com.pragma.entidad;

public enum EstadoCivil {

    SOLTERO_A,
    CASADO_A,
    UNION_MARITAL_DE_HECHO;

    public static EstadoCivil convertirEstadoCivil(String estadoCivil) throws Exception{
        if(estadoCivil.isEmpty() || estadoCivil == null){
            return null;
        }

        if(estadoCivil.equalsIgnoreCase("Unión marital de hecho")){
            return UNION_MARITAL_DE_HECHO;

        }
        if(estadoCivil.equalsIgnoreCase("Soltero/a") || estadoCivil.equalsIgnoreCase("Soltera")) {
        	return SOLTERO_A;
        }
        
        if(estadoCivil.equalsIgnoreCase("Soltero") || estadoCivil.equalsIgnoreCase("Soltera")) {
        	return SOLTERO_A;
        }
        
        if(estadoCivil.equalsIgnoreCase("Casado/a") || estadoCivil.equalsIgnoreCase("Soltera")) {
        	return CASADO_A;
        }
        
        if(estadoCivil.equalsIgnoreCase("Casado") || estadoCivil.equalsIgnoreCase("Casada")) {
        	return CASADO_A;
        }

        for (EstadoCivil value : EstadoCivil.values()) {
            if(value.name().equalsIgnoreCase(estadoCivil)){

                return value;
            }

        }
        throw new Exception("Ocurrio un error con el estado civil" );

    }

}
