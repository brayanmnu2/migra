package com.pragma.entidad;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "ubicacion_geografica")
@Getter
@Setter
public class UbicacionGeografica {
	
	//TODO: cambiar column name a "id" cuando se suba a dev o prod
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_ubicacion" , unique = true , nullable = false)
    private Long idUbicacion;

    @Column(name = " id_legacy", nullable = false)
    private String idLegacy;

    @Column(name = "nombre" , nullable = false)
    private String nombre;

    @Column(name = "padre")
    private String padre;

    @Column(name = "nivel" , nullable = false)
    private String nivel;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "estado" , nullable = false)
    private Estado estado;

    @Column(name = "codigo" , nullable = false)
    private String codigo;

    @Column(name = "indicativo" , nullable = false)
    private String indicativo;

    @OneToMany(fetch = FetchType.LAZY , mappedBy = "idLugarDeNacimiento")
    private List<DatosPersonales> listDatosNacimiento = new ArrayList<>();

    @OneToMany(fetch = FetchType.LAZY , mappedBy = "idLugarDeResidencia")
    private List<DatosPersonales> listDatosResidencia = new ArrayList<>();
    
    @OneToMany(fetch = FetchType.LAZY , mappedBy = "nacionalidad")
    private List<DatosPersonales> listDatosNacionalidad = new ArrayList<>();


}
