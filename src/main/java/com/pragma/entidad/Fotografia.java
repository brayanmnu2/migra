package com.pragma.entidad;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "fotografia")
public class Fotografia {
	
	@Id
    @Column(name = "id" , unique = true , nullable = false , length = 50)
    private Long id;

    @Column(name = "url" , nullable = false , length = 50)
    private String url;

    @JoinColumn(name = "tipo_fotografia")
    private TipoFotografia tipoFotografia;

    @OneToMany()
    private List<DatosPersonales> usuario = new ArrayList<>();

}
