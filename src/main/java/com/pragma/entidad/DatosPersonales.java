package com.pragma.entidad;

import java.time.LocalDate;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "datos_personales")
@Getter
@Setter
public class DatosPersonales {
	
	@Id
	@Column(name = "id")
	@GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
	private String id;
	
   
    @Column(name = "google_id" , unique = true , nullable = false , length = 50)
    private String googleId;

    @Column(name = "nombres" , length = 50)
    private String nombres;
    
    /*
    @Column(name = "aceptacion_de_terminos")
    private int aceptacionDeTerminos;*/
 
    
    @Column(name = "identificacion" , length = 50)
    private String identificacion;

    @Column(name = "apellidos" , length = 50)
    private String apellidos;
   
    @Column(name = "correo_personal" ,  length = 55)
    private String correoPersonal;

    @Column(name = "correo_empresarial" ,  length = 55)
    private String correoEmpresarial;

    @Column(name = "telefono_fijo" , length = 55)
    private String telefonoFijo;

    @Column(name = "telefono_celular" , length = 55)
    private String telefonoCelular;

    @Column(name = "fecha_nacimiento")
    private LocalDate fechaNacimiento;
    
    @Column(name = "direccion")
    private String direccion;

    @Column(name = "nombre_contacto_emergencia" , length = 55)
    private String nombreContactoEmergencia;

    @Column(name = "parentesco_contacto_emergencia" , length = 55)
    private String parentescoContactoEmergencia;

    @Column(name = "telefono_contacto_emergencia" ,  length = 55)
    private String telefonoContactoEmergencia;

    @Column(name = "tarjeta_profesional" , length = 55)
    private String tarjetaProfesional;
    /*
    @Column(name = "dias_tomados")
    private Integer diasTomados;
    
    @Column(name = "dias_ganados")
    private Integer diasGanados;
    */
    
    @Column(name = "perfil_profesional" ,  length = 255)
    private String perfilProfesional;

    @Column(name = "conocimientos_tecnicos")
    private String conocimientosTecnicos;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "talla_camisa" )
    private TallaCamisa tallaCamisa;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "estado_civil")
    private EstadoCivil estadoCivil;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "grupo_sanguineo")
    private GrupoSanguineo grupoSanguineo;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "estado")
    private Estado estado;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_tipo_documento")
    private TipoDocumento idTipoDocumento;
    
    //Falta la nacionalidad
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_profesion")
    private Profesion idProfesion;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_lugar_de_nacimiento" ) 
    private UbicacionGeografica idLugarDeNacimiento;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_lugar_de_residencia")
    private UbicacionGeografica idLugarDeResidencia; 
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "nacionalidad")
    private UbicacionGeografica nacionalidad;

    @Column(name = "fecha_ultima_actualizacion") 
    private LocalDateTime fechaUltimaActualizacion;
    /*
    @Column(name = "fecha_creacion")
    private LocalDate fechaCreacion; */
    
    @Column(name = "fecha_ingreso")
    private LocalDate fechaIngreso; 
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_vicepresidencia")
    private Vicepresidencia idVicepresidencia;
    
    
    
}
