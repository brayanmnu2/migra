package com.pragma.entidad;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;

@Entity
@Table(name = "parentesco")
@Getter
public class Parentesco {
	
	@Id
    @Column(name = "id_parentesco", unique = true , nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idParentesco;

    @Column(name = "nombre" , unique = true , nullable = false)
    private String nombre;

}
