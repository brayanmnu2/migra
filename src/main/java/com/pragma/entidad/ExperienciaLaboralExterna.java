package com.pragma.entidad;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "experiencia_laboral_externa")
@Getter
@Setter
public class ExperienciaLaboralExterna {
	
	@Id
	@Column(name = "id")
	@GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
	private String id;
	
	@Column(name = "empresa" , length = 255)
	private String empresa;
	
	@Column(name = "rol" , length = 255)
	private String rol;
	
	@Column(name = "fecha_de_ingreso")
	private LocalDate fechaDeIngreso;
	
	@Column(name = "fecha_de_finalizacion")
	private LocalDate fechaDeFinalizacion;
	
	@Column(name = "tiempo_de_experiencia")
	private String tiempoDeExperiencia;
	
	@Column(name = "logros")
	private String logros;
	
	@Column(name = "id_usuario" , length = 255)
	private String idUsuario;

}
