package com.pragma.entidad;

public enum GrupoSanguineo {
	O_POSITIVO, O_NEGATIVO, A_POSITIVO, A_NEGATIVO, B_POSITIVO, B_NEGATIVO, AB_POSITIVO, AB_NEGATIVO, NO_ESPECIFICA;

	private final static String SEPARADOR = "_";

	public static String rhFormateado(String rh) {
		rh = rh.toLowerCase();
		if (rh.equalsIgnoreCase("+")) {
			return "positivo";
		} else if (rh.equalsIgnoreCase("-")) {
			return "negativo";

		}

		return null;
	}

	public static GrupoSanguineo convertir(String tipo, String rh) {
		GrupoSanguineo grupoSanguineo = GrupoSanguineo.NO_ESPECIFICA;

		// String dato[] = grupoSanguineo.name().split(SEPARADOR);
		// String tipoAuxiliar = dato[0];
		
		
		if (!rh.isBlank()) {

			String rhAuxiliar = rhFormateado(rh).toUpperCase();
			
			grupoSanguineo =GrupoSanguineo.valueOf(tipo.toUpperCase()+SEPARADOR+rhAuxiliar);
		}
		/*
		 * if(tipo.equalsIgnoreCase(tipo) && rhAuxiliar.equalsIgnoreCase(rh)){ return
		 * grupoSanguineo; }
		 */

		return grupoSanguineo;
		// throw new InvalidEnumSangreValueException("tipo" , "rh" , tipo , rh);

	}

}
