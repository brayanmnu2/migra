package com.pragma.entidad;

public enum TallaCamisa {

	DIECISEIS,
    L ,
    M ,
    S ,
    XL,
    XS ,
    XXL ,
    XXXL ,
    XXXXL;

    public static TallaCamisa convertirTallaCamisa(String talla) throws Exception{
        if(talla.isEmpty() || talla == null){
            return null;
        }

        if(talla.equals("16")){
            return TallaCamisa.DIECISEIS;
        }

        for (TallaCamisa value : TallaCamisa.values()) {
            if(value.name().equalsIgnoreCase(talla)){
                return value;
            }

        }
        throw new Exception("Ocurrio un error con la talla de camisa");

    }

}
