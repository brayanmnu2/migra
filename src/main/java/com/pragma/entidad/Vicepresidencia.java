package com.pragma.entidad;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "vicepresidencia")
@Getter
@Setter
public class Vicepresidencia {
	 
	@Id
	private Long id;
	
	private String nombre;

	@OneToMany(mappedBy = "idVicepresidencia")
    private List<DatosPersonales> usuario = new ArrayList<>();
}
