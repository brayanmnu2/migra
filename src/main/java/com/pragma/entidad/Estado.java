package com.pragma.entidad;

public enum Estado {
	ACTIVO, INACTIVO;

	public static Estado convertirEstado(String estado) throws Exception {
		if (estado.equalsIgnoreCase("true")) {
			return Estado.ACTIVO;
		} else if (estado.equalsIgnoreCase("false")) {
			return Estado.INACTIVO;

		}
	   throw new Exception("Ocurrio un error con el estado");
	}

}
