package com.pragma.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pragma.entidad.ExperienciaLaboralPragma;

public interface ExperienciaLaboralPragmaRepository extends JpaRepository<ExperienciaLaboralPragma, String> {

}
