package com.pragma.repositorio;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pragma.entidad.DatosPersonales;

public interface DatosPersonalesRepository extends JpaRepository<DatosPersonales , String> {
	
	Optional <DatosPersonales> findByGoogleId(String googleId);
	
	Optional <DatosPersonales> findByCorreoEmpresarial(String correoEmpresarial);

	
	
}
