package com.pragma.repositorio;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pragma.entidad.Parentesco;

public interface ParentescoRepository extends JpaRepository<Parentesco , Long>{
	
	Optional<Parentesco> findByNombre(String nombre);

}
