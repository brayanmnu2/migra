package com.pragma.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pragma.entidad.OtrosEstudios;

public interface OtrosEstudiosRepository extends JpaRepository<OtrosEstudios, String> {

}
