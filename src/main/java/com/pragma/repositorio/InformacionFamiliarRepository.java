package com.pragma.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pragma.entidad.InformacionFamiliar;

public interface InformacionFamiliarRepository extends JpaRepository<InformacionFamiliar, String> {

}
