package com.pragma.repositorio;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pragma.entidad.Vicepresidencia;

public interface VicepresidenciaRepository extends JpaRepository<Vicepresidencia , Long>{
	
    Optional<Vicepresidencia> findByNombre(String nombre);


}
