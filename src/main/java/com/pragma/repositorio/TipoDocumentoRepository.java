package com.pragma.repositorio;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pragma.entidad.TipoDocumento;


public interface TipoDocumentoRepository extends JpaRepository<TipoDocumento, Long> {
	
    Optional<TipoDocumento> findByNombre(String nombre);


}
