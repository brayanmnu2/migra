package com.pragma.repositorio;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pragma.entidad.Profesion;


public interface ProfesionRepository extends JpaRepository<Profesion , Long> {
	
    Optional<Profesion> findByNombre(String nombre);


}
