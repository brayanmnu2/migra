package com.pragma.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pragma.entidad.Estudio;

public interface EstudioRepository extends JpaRepository<Estudio, String> {

}
