package com.pragma.repositorio;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.pragma.entidad.UbicacionGeografica;


public interface UbicacionGeograficaRepository extends JpaRepository<UbicacionGeografica, Long> {

	@Query("SELECT t FROM UbicacionGeografica t WHERE LOWER(t.idLegacy) = LOWER(:idLegacy)")
	Optional<UbicacionGeografica> findByLegacy(String idLegacy);
	

}
