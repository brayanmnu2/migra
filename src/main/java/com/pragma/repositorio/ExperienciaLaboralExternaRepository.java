package com.pragma.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pragma.entidad.ExperienciaLaboralExterna;

public interface ExperienciaLaboralExternaRepository extends JpaRepository<ExperienciaLaboralExterna, String> {

}
