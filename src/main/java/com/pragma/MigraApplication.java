package com.pragma;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.pragma.servicio.MigracionEstudioServicio;
import com.pragma.servicio.MigracionExperienciaLaboralExternaServicio;
import com.pragma.servicio.MigracionExperienciaLaboralPragmaServicio;
import com.pragma.servicio.MigracionInformacionFamiliarServicio;
import com.pragma.servicio.MigracionInformacionPersonalServicio;
import com.pragma.servicio.MigracionOtrosEstudiosServicio;

@SpringBootApplication
public class MigraApplication implements CommandLineRunner {

	@Autowired
	MigracionExperienciaLaboralPragmaServicio mG;
	
	@Autowired
	MigracionExperienciaLaboralExternaServicio mE;
	
	@Autowired
	MigracionInformacionPersonalServicio mP;
	
	@Autowired
	MigracionInformacionFamiliarServicio mF;
	
	@Autowired
	MigracionEstudioServicio mEs;
	
	@Autowired
	MigracionOtrosEstudiosServicio mOE;

	public static void main(String[] args) {
		SpringApplication.run(MigraApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
		/*MIGRAR USUARIOS*/
		mP.migrarPragmaticos();
		
		/*MIGRAR INFORMACION FAMILIAR*/
	    //mF.migrarInformacionFamiliar(); 
		
		/*MIGRAR INFORMACION LABORAL EXTERNA*/
		//mE.migrarInformacionLaboralExterna();
		
		/*MIGRAR INFORMACION LABORAL PRAGMA*/
		//mG.migrarInformacionLaboralPragma();
		
		/*MIGRAR ESTUDIO*/
		//mEs.migrarEstudio(); //////YA
		
		/*MIGRAR OTROS ESTUDIOS*/
		//mOE.migrarOtrosEstudios();

	}

}
